import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
//    static final DecimalFormat df = new DecimalFormat("0.0000");

    public static List<Node> nodes = new ArrayList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int nodeCount = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < nodeCount; i++) {
            Node node = new Node();
            String line = scanner.nextLine();
            node.processLine(line);
            nodes.add(node);
        }

        int evidenceCount = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < evidenceCount; i++) {
            String[] split = scanner.nextLine().split("\t");
            nodes.get(Integer.parseInt(split[0])).value = Integer.parseInt(split[1]);
        }

        int destinationIndex = scanner.nextInt();

        Decision decision = new Decision();

        decision.availableDecisions = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < nodes.get(destinationIndex).availableValues * decision.availableDecisions; i++) {
            decision.fillTableOneLine(scanner.nextLine());
        }

        ///////////////// read /////////////////

        int variations = 1;
        for (Node node : nodes) {
            if (node.value == -1) {
                variations *= node.availableValues;
            }
        }

        int[] sizes = new int[nodes.size()];
        int[][] actualValues = new int[variations][nodes.size()];
        int[] row = new int[nodes.size()];


        for (int i = 0; i < nodes.size(); i++) {
            sizes[i] = nodes.get(i).availableValues;
        }

        for (int i = 0; i < variations; i++) {
            for (int j = sizes.length - 1; j >= 0; j--) {
                if (nodes.get(j).value != -1) {
                   row[j] = nodes.get(j).value;
                } else {
                    if (row[j] < sizes[j] - 1) {
                        row[j]++;
                        break;
                    }

                    row[j] = 0;
                }
            }
            actualValues[i] = row.clone();
        }


        double[] results = new double[nodes.get(destinationIndex).availableValues];


        for (int i = 0; i < actualValues.length; i++) {
            Double probability = 1.0;
            for (int j = 0; j < nodes.size(); j++) {
                ArrayList<Integer> indexes = new ArrayList<>();
                Node node = nodes.get(j);

                for (int k = 0; k < node.parentsIndexes.size(); k++) {
                    indexes.add(actualValues[i][node.parentsIndexes.get(k)]);
                }
                probability *= node.getProbabilityByValues(indexes, actualValues[i][j]);
            }
            results[actualValues[i][destinationIndex]] += probability;
        }

        double sum = 0.0;
        for (double result : results) {
            sum += result;
        }
        for (int i = 0; i < results.length; i++) {
            results[i] /= sum;
        }

        decision.calculateValue(results, nodes.get(destinationIndex));



        for (double result : results) {
//            System.out.println(df.format(result));
            System.out.println(result);
        }
        System.out.println(decision.value);
    }
}
