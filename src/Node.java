import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Node {
    public int id;
    public static int idCounter = 0;
    public int availableValues;
    public int value = -1;
    public HashMap<ArrayList<Integer>, ArrayList<Double>> probabilityBoard = new HashMap<>();
    public ArrayList<Integer> parentsIndexes = new ArrayList<>();
    public HashMap<Integer, Double> likelihoods = new HashMap<>();

    public Node() {
        id = idCounter++;
    }

    private void fillProbabilityBoardOneLine(int parentsCount, String line) {
        if (parentsCount == 0) {
            Double[] probabilities = Arrays.stream(line.split(",")).map(Double::valueOf).toArray(Double[]::new);
            probabilityBoard.put(new ArrayList<Integer>(), new ArrayList<>(Arrays.asList(probabilities)));
        } else {
            String[] valuesArray = line.split(":");
            Integer[] indexes = Arrays.stream(valuesArray[0].split(",")).map(Integer::parseInt).toArray(Integer[]::new);
            Double[] probabilities = Arrays.stream(valuesArray[1].split(",")).map(Double::valueOf).toArray(Double[]::new);
            probabilityBoard.put(new ArrayList<>(Arrays.asList(indexes)), new ArrayList<>(Arrays.asList(probabilities)));
        }
    }

    public void processLine(String line) {
        int index = 0;
        String[] split = line.split("\t");
        availableValues = Integer.parseInt(split[index++]);
        int parentsCount = Integer.parseInt(split[index++]);

        for (int i = 0; i < parentsCount; i++) {
            parentsIndexes.add(Integer.parseInt(split[index++]));
        }

        for (int i = index; i < split.length; i++) {
            fillProbabilityBoardOneLine(parentsCount, split[i]);
        }
    }

    public Double getProbabilityByValues(ArrayList<Integer> indexes, int currentNodeIndex) {
        if (parentsIndexes.isEmpty()) {
            return probabilityBoard.get(new ArrayList<Integer>()).get(currentNodeIndex);
        }
        return probabilityBoard.get(indexes).get(currentNodeIndex);
    }

    public void calculateLikelihood() {
        if (value != -1) {
            // TODO: neki van egy fix értéke
        }
        if (parentsIndexes.isEmpty()) {
            // nincsenek szülei -> csak magától függ
            ArrayList<Double> probabilities = probabilityBoard.get(new ArrayList<>());
            for (int i = 0; i < probabilities.size(); i++) {
                likelihoods.put( i, probabilities.get(i));
            }
            return;
        }



    }
}
