import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Decision {
    public int availableDecisions;
    public int value;
    public HashMap<ArrayList<Integer>, Double> table = new HashMap<>();

    public void fillTableOneLine(String line) {
        String[] split = line.split("\t");
        table.put(new ArrayList<Integer>(Arrays.asList(Integer.parseInt(split[0]), Integer.parseInt(split[1]))),
                Double.parseDouble(split[2]));
    }

    public void calculateValue(double[] results, Node target) {
        double[] utilities = new double[availableDecisions];

        for (int i = 0; i < utilities.length; i++) {
            for (int j = 0; j < results.length; j++) {
                utilities[i] += table.get(new ArrayList<>(Arrays.asList(j, i))) * results[j];
            }
        }

        int maxIndex = 0;
        for (int i = 0; i < utilities.length; i++) {
            if (utilities[i] > utilities[maxIndex])
                maxIndex = i;
        }
        value = maxIndex;
    }
}
